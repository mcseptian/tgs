import React from 'react';

import classNames from 'classnames';

import { ITextFieldProps } from '@textfield';

type ReactTextarea = React.TextareaHTMLAttributes<HTMLTextAreaElement> & HTMLTextAreaElement;

type TextAreaProps = ITextFieldProps & Omit<ReactTextarea, keyof ITextFieldProps>;

if (process.env.webpack) {
    require('./textarea.scss');
}

export default class TextArea extends React.Component<TextAreaProps, {}> {
    render(): JSX.Element {
        const className = classNames('textarea', this.props.modifier, this.props.className);

        return (
            <textarea
                {...this.props}
                className={className}
            />
        );
    }
}
